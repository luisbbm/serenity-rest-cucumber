# Example REST API testing with Serenity and Cucumber 6

This is a Rest Assured BDD framework for API test automation. Took the approach of BDD along with Rest Assured in order to ensure that the specifications are testable. When the features/scenarios are written in Gherkin and glued with step definitions.

## API Server used:

https://any-api.com/refugerestrooms_org/refugerestrooms_org/console/restrooms

API documentation: https://www.refugerestrooms.org/api/docs/

## Run Instructions:

Run it as 'mvn clean verify' from the project dir

## Living documentation

You can generate full Serenity reports by running `mvn clean verify`. 
This includes both the living documentation from the feature files

And also details of the REST requests and responses that were executed during the test
