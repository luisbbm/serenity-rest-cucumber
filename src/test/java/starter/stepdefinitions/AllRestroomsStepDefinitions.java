package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static starter.WebServiceEndPoints.ALL_RESTROOMS;

public class AllRestroomsStepDefinitions {
    private static final int EXPECTED_AMOUNT_RESTROOMS = 10;

    private static Response response;
    private static String jsonString;
    private static int amountOfRestrooms;

    @When("a user search for restrooms")
    public void a_user_search_for_restrooms() {

        response = RestAssured.get(ALL_RESTROOMS.getUrl());
        jsonString = response.asString();
        List<Map<String, String>> restrooms = JsonPath.from(jsonString).get("id");
        amountOfRestrooms = restrooms.size();
    }

    @Then("a list of ten restrooms is displayed")
    public void a_list_of_ten_restrooms_is_displayed() {
        Assert.assertTrue(amountOfRestrooms == EXPECTED_AMOUNT_RESTROOMS);
    }
}
