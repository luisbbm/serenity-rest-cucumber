package starter;

public enum WebServiceEndPoints {
    ALL_RESTROOMS("https://www.refugerestrooms.org/api/v1/restrooms"),
    BY_LOCATION("https://www.refugerestrooms.org/api/v1/restrooms/by_location"),
    BY_DATE("https://www.refugerestrooms.org/api/v1/restrooms/by_date"),
    QUERY_SEARCH("https://www.refugerestrooms.org/api/v1/restrooms/search");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
