Feature: Get restrooms for a geographic position
  Description: Search by location.

  Scenario: List of restrooms in Amsterdam
    When a user search with the geographic coordinates of Amsterdam
    Then the list of restrooms are in Amsterdam