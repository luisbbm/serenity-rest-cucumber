Feature: Get all restrooms
  Description: Get all restroom records ordered by date descending.

  Scenario: List of the latest 10 restrooms
    When a user search for restrooms
    Then a list of ten restrooms is displayed

  Scenario: List restrooms for more than 100 per page
    When a user search more than 100 results per page
    Then a message notifying maximum 100 results per page

  Scenario: List only unisex restrooms
    When a user search restrooms filtering by unisex
    Then the list of results only display unisex restrooms

  Scenario: List only non-unisex restrooms
    When a user search restrooms filtering by non-unisex
    Then only non-unisex restrooms are displayed

  Scenario: List only ADA accessible restrooms
    When a user search restrooms filtering by ADA accessible
    Then all results displayed are ADA accessible restrooms

  Scenario: List only non-ADA accessible restrooms
    When a user search restrooms filtering by non-ADA Accessible
    Then the list of results only display restrooms non-ADA Accessible